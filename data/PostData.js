import { USERS } from "./UserData";

export const POSTS = [
    {
        imageUrl: 'https://img.dummyapi.io/photo-1568480541687-16c2f73eea4c.jpg',
        user: USERS[0].user,
        likes: 2555556,
        caption: 'This is caption 1 for the user name nand kumar how are you doing bro',
        profile_picture: USERS[0].image,
        comments: [
            {
                user: "Nand kumar",
                comment: "This is comment 1"
            },
            {
                user: "Nand kumar",
                comment: "This is comment 2"
            },
            {
                user: "Nand kumar",
                comment: "This is comment 3"
            }
        ]

    },
    {
        imageUrl: 'https://png.pngtree.com/element_our/sm/20180524/sm_5b072d393d61e.jpg',
        user: USERS[1].user,
        likes: 25,
        caption: 'This is caption 1',
        profile_picture: USERS[1].image,
        comments: [
            {
                user: "Nand kumar",
                comment: "This is comment 1"
            }
        ]

    },
    {
        imageUrl: 'https://img.dummyapi.io/photo-1568480541687-16c2f73eea4c.jpg',
        user: USERS[3].user,
        likes: 255,
        caption: 'This is caption 1',
        profile_picture: USERS[3].image,
        comments: [
            {
                user: "Nand kumar",
                comment: "This is comment 1"
            }
        ]

    },
    {
        imageUrl: 'https://img.dummyapi.io/photo-1548658146-f142deadf8f7.jpg',
        user: USERS[5].user,
        likes: 255,
        caption: 'This is caption 1',
        profile_picture: USERS[5].image,
        comments: [
            {
                user: "Nand kumar",
                comment: "This is comment 1"
            }
        ]

    },
]