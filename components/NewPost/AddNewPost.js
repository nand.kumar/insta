import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import React from 'react'
import { AntDesign } from '@expo/vector-icons';
import FormikPostUploader from './FormikPostUploader';

const AddNewPost = ({ navigation }) => (

    <View style={styles.container}>
        <Header navigation={navigation} />
        <FormikPostUploader navigation={navigation} />
    </View>
)

const Header = ({ navigation }) => (
    <View style={styles.headerContainer}>
        <TouchableOpacity>
            <AntDesign onPress={() => navigation.goBack()} name="back" size={24} color="white" />
        </TouchableOpacity>
        <Text style={styles.headerText}>New Post</Text>
        <Text></Text>
    </View>
)

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 10
    },
    headerContainer: {
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center'
    },
    headerText: {
        color: "#fff",
        fontWeight: '700',
        fontSize: 20,
        marginRight: 23
    }

})
export default AddNewPost