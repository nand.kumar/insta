import { View, Text, TextInput, Button, StyleSheet, Pressable, TouchableOpacity, Alert } from 'react-native'
import React, { useState } from 'react'
import firebase from '../../Firebase';
import * as yup from 'yup';
import { signInWithEmailAndPassword } from "firebase/auth";
import { Formik, Field, Form } from "formik";
import { validate } from 'email-validator';
import { auth } from '../../Firebase';


const LoginForm = ({ navigation }) => {
    const loginFormSchema = yup.object().shape({
        email: yup.string().email().required('An email is Required'),
        password: yup.string().required().min(8, 'Passowrd should be of minimum 8 characters')
    });

    const onLogin = async (email, password) => {
        try {

            const userCredential = await signInWithEmailAndPassword(auth, email, password)
            // Signed in 
            const user = userCredential.user;
            console.log(user, "Logged in succesfully")
            // ...

            // await firebase.auth().signInWithEmailAndPassword(email, password);
            console.log("Firebase login succesfull")
        } catch (error) {
            console.log(error)
            Alert.alert('My Lord',
                error.message + '\n\n ... What would you like to do next..?',
                [
                    {
                        text: 'ok',
                        onPress: () => console.log('ok'),
                        style: 'cancel'
                    },
                    {
                        text: 'Sign up',
                        onPress: () => navigation.push('SignupScreen'),
                        style: 'cancel'
                    }
                ])
        }

    }
    return (
        <View style={{ marginTop: 80 }}>
            <Formik

                initialValues={{ email: "", password: "" }}
                onSubmit={async (values) => {
                    await onLogin(values.email, values.password);
                    console.log(values);
                    // navigation.navigate('HomeScreen');
                }
                }
                validationSchema={loginFormSchema}
                validateOnMount={true}
            >
                {({ handleBlur, handleChange, handleSubmit, values, errors, isValid }) =>

                    <>

                        <View style={[styles.inputField, { borderColor: values.email.length < 1 || validate(values.email) ? "#ccc" : "red" }]}>
                            <TextInput
                                placeholderTextColor="#444"
                                placeholder='Phone , Email ,Username'
                                autoCapitalize='none'
                                keyboardType='email-address'
                                textContentType='emailAddress'
                                autoFocus={true}
                                onChangeText={handleChange("email")}
                                onBlur={handleBlur("email")}
                                value={values.email}
                            />
                        </View>
                        <View style={[styles.inputField, {
                            borderColor: 1 > values.password.length || values.password.length >= 8 ? "#ccc" : "red"
                        }]}>
                            <TextInput

                                placeholderTextColor="#444"
                                placeholder='Password'
                                autoCapitalize='none'
                                textContentType='password'
                                autoCorrect={false}
                                secureTextEntry={true}
                                onChangeText={handleChange("password")}
                                onBlur={handleBlur("password")}
                                value={values.password}
                            />
                        </View>
                        <View style={{ marginBottom: 30, alignItems: 'flex-end', marginEnd: 10 }}>
                            <Text style={{ color: '#6bb0f5' }} >
                                forgot password...?
                            </Text>
                        </View>
                        <Pressable titleSize={20} style={styles.button(isValid)} onPress={handleSubmit} disabled={!isValid}>
                            <Text style={styles.buttonText}>

                                Log In
                            </Text>
                        </Pressable>

                        <View style={styles.signupContainer}>
                            <Text>Don't have a account ? </Text>
                            <TouchableOpacity onPress={() => navigation.push('SignupScreen')}>
                                <Text style={{ color: '#6bb0f5' }}> Sign Up</Text>
                            </TouchableOpacity>
                        </View>
                    </>
                }
            </Formik>
        </View >
    )
}

const styles = StyleSheet.create({
    inputField: {
        borderRadius: 4,
        padding: 12,
        backgroundColor: "#FAFAFA",
        marginBottom: 10,
        borderWidth: 1
    },
    button: isValid => ({
        backgroundColor: isValid ? '#0096f6' : "#9acaf7",
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: 42,
        borderRadius: 4
    })
    ,
    buttonText: {
        color: '#fff',
        fontWeight: '600',
        fontSize: 20
    },
    signupContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 50
    }
})
export default LoginForm