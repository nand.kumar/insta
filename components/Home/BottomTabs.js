import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native'
import React, { useState } from 'react'
import { Feather, Entypo, Ionicons, AntDesign, FontAwesome } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Divider } from 'react-native-elements';

// const IconsArray = [
//     {
//         name: "Home",
//         active: <Feather name="home" size={24} color="white" style={styles.icons} />,
//         inActive: <Entypo name="home" size={24} color="white" style={styles.icons} />

//     }
// ]

const BottomTabs = () => {
    const [activeTab, setActiveTab] = useState("Home");



    const Icons = () => (
        <View style={styles.container}>
            {activeTab === "Home" ?
                <TouchableOpacity onPress={() => setActiveTab("Home")}>
                    <Entypo name="home" size={24} color="white" style={styles.icons} />
                </TouchableOpacity> : <TouchableOpacity onPress={() => setActiveTab("Home")}>
                    <Feather name="home" size={24} color="white" style={styles.icons} />
                </TouchableOpacity>}
            {activeTab === "Search" ? <TouchableOpacity onPress={() => setActiveTab("Search")}>
                <FontAwesome name="search" size={24} color="white" style={styles.icons} />
            </TouchableOpacity> :
                <TouchableOpacity onPress={() => setActiveTab("Search")}>
                    <AntDesign name="search1" size={24} color="white" style={styles.icons} />
                </TouchableOpacity>}



            {activeTab === "Reels" ? <TouchableOpacity onPress={() => setActiveTab("Reels")}>
                <Ionicons name="videocam-sharp" size={24} color="white" style={styles.icons} />
            </TouchableOpacity> :
                <TouchableOpacity onPress={() => setActiveTab("Reels")}>
                    <Ionicons name="videocam-outline" size={24} color="white" style={styles.icons} />
                </TouchableOpacity>}



            {activeTab === "Shopping" ? <TouchableOpacity onPress={() => setActiveTab("Shopping")}>
                <Entypo name="shopping-bag" size={24} color="white" style={styles.icons} />
            </TouchableOpacity> :
                <TouchableOpacity onPress={() => setActiveTab("Shopping")}>
                    <Feather name="shopping-bag" size={24} color="white" style={styles.icons} />
                </TouchableOpacity>}

            <TouchableOpacity onPress={() => setActiveTab("Profile")}>
                <Image source={{
                    uri: 'https://www.nandkumar.me/static/media/mypic.cf39c9af.jpg'
                }}
                    style={[styles.icons, styles.profilePic(activeTab)]}
                />
            </TouchableOpacity>
        </View>
    )
    return (
        <View style={styles.wrapper}>
            <Divider width={1} orientation='vertical' />
            <Icons />
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        position: 'absolute',
        width: '100%',
        bottom: 0,
        zIndex: 999,
        backgroundColor: "#000"
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: 60,
        paddingTop: 10,
        alignItems: 'center'
    },
    icons: {
        width: 40,
        height: 40
    },
    profilePic: (activeTab = '') => ({
        borderRadius: 100,
        borderColor: 'white',
        borderWidth: activeTab === "Profile" ? 2 : 0
    }),
})

export default BottomTabs