import { View, Text, Image, StyleSheet, TouchableOpacity, Alert } from 'react-native'
import React from 'react';
import { FontAwesome5 } from '@expo/vector-icons';
import { AntDesign, MaterialIcons } from '@expo/vector-icons';
import { auth } from '../../Firebase';
import { signOut } from "firebase/auth";

const Header = ({ navigation }) => {

  const handleSignOut = async () => {
    try {
      await signOut(auth);
      console.log("Logged out succesfully")
    } catch (error) {
      Alert.alert('My Lord',
        error.message,)
    }
  }
  return (
    <View style={styles.container}>
      <TouchableOpacity>

        <Image
          style={styles.logo}
          source={require('../../assets/header-logo.png')}
        />
      </TouchableOpacity>

      <View style={styles.iconsContainer}>
        <TouchableOpacity onPress={() => navigation.navigate('NewPostScreen')}>
          <AntDesign name="plussquareo" size={24} color="white" style={styles.icon} />
        </TouchableOpacity>

        <TouchableOpacity onPress={handleSignOut}>
          <MaterialIcons name="logout" size={24} color="white" style={styles.icon} />
        </TouchableOpacity>

        <TouchableOpacity>
          <View style={styles.unreadBadge}>
            <Text style={styles.unreadBadgeText}>2</Text>
          </View>
          <FontAwesome5 name="facebook-messenger" size={24} color="white" style={styles.icon} />

        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({

  container: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: 20
  },
  logo: {
    width: 110,
    height: 50,
    resizeMode: 'contain'
  },
  iconsContainer: {
    flexDirection: 'row'
  },
  icon: {
    height: 30,
    width: 30,
    marginLeft: 10,
    resizeMode: 'contain'
  },
  unreadBadge: {
    backgroundColor: "#FF3250",
    position: "absolute",
    left: 20,
    bottom: 18,
    width: 25,
    height: 18,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 100
  },
  unreadBadgeText: {
    color: 'white',
    fontWeight: '600'
  }
})

export default Header