import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import { Divider } from 'react-native-elements'
import { FontAwesome5 } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { auth, db } from '../../Firebase';
import { arrayRemove, arrayUnion, doc, updateDoc } from 'firebase/firestore';

const Post = ({ post, getAllPosts }) => {


    const handleLike = async (post) => {
        try {
            console.log(post)
            const currentLikeStatus = !post.likes_by_user.includes(auth.currentUser.email);
            console.log(currentLikeStatus)

            console.log(post.owner_email, post.id)
            const postRef = doc(db, "users", post.owner_email, "posts", post.id);
            if (currentLikeStatus) {

                await updateDoc(postRef, {
                    likes_by_user: arrayUnion(auth.currentUser.email)
                });
            }
            else {

                await updateDoc(postRef, {
                    likes_by_user: arrayRemove(auth.currentUser.email)
                });
            }
            getAllPosts();
        } catch (error) {
            console.log(error)
        }
    }


    return (
        <View style={{ marginBottom: 30 }}>
            <Divider width={1} orientation='vertical' />
            <PostHeader post={post} />
            <PostImage post={post} getAllPosts={getAllPosts} />
            <View style={{ marginTop: 10, marginHorizontal: 15 }}>
                <PostFooter post={post} handleLike={handleLike} />
                <Likes post={post} />
                <Caption post={post} />
                <CommentsSection post={post} />
                <Comments post={post} />
            </View>
        </View>
    )
}

const PostHeader = ({ post }) => (
    <View
        style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            margin: 6
        }}
    >
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image source={{ uri: post.profile_picture }} style={styles.story} />
            <Text style={{ color: 'white', marginLeft: 5, fontWeight: '700' }}>{post.user}</Text>
        </View>
        <Text style={{ color: 'white', fontWeight: '900' }}>...</Text>
    </View>
)

const PostImage = ({ post }) => (
    <View
        style={{
            height: 450,
            width: '100%'
        }}
    >

        <Image source={{
            uri: post.imageUrl
        }}
            style={styles.postImage}

        />
    </View>

)

const PostFooter = ({ post, handleLike }) => (
    <View style={{
        flexDirection: 'row'
    }}>
        <View style={styles.leftFooterIconsContainer}>

            {!post.likes_by_user.includes(auth.currentUser.email) ? <TouchableOpacity onPress={() => handleLike(post)}>
                <FontAwesome5 name="heart" size={24} color="white" style={styles.icon} />
            </TouchableOpacity>
                :
                <TouchableOpacity onPress={() => handleLike(post)}>
                    <FontAwesome name="heart" size={24} color="red" style={styles.icon} />
                </TouchableOpacity>}
            <TouchableOpacity>
                <FontAwesome5 name="comment" size={24} color="white" style={styles.icon} />
            </TouchableOpacity>
            <TouchableOpacity>
                <FontAwesome name="send" size={24} color="white" style={styles.icon} />
            </TouchableOpacity>
        </View>
        <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <TouchableOpacity>
                <FontAwesome name="save" size={24} color="white" style={styles.icon} />
            </TouchableOpacity>

        </View>

    </View>
)


const Likes = ({ post }) => (
    <View style={{ flexDirection: 'row', marginTop: 4 }}>

        <Text style={{ color: 'white', fontWeight: '600' }}>{post.likes_by_user.length.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} likes</Text>
    </View>
)


const Caption = ({ post }) => (
    <View style={{ marginTop: 5 }}>

        <Text style={{ color: 'white' }} >
            <Text style={{ fontWeight: '900' }}>{post.user}</Text>
            <Text> {post.caption}</Text>

        </Text>
    </View>
)


const CommentsSection = ({ post }) => (
    <View style={{ marginTop: 5 }}>

        {post.comments && !!post.comments.length && (<Text style={{ color: 'gray' }}>

            View {post.comments.length > 1 ? 'all' : ''} {post.comments.length}{' '}
            {post.comments.length > 1 ? "comments" : "comment"}
        </Text>)}
    </View>
);


const Comments = ({ post }) => (
    <>
        {
            post.comments && post.comments.map((comment, index) => (
                <View key={index} style={{ flexDirection: 'row', marginTop: 5 }}>
                    <Text style={{ color: 'white' }} >
                        <Text style={{ fontWeight: '900' }}>{comment.user}</Text>
                        <Text>{' '} {comment.comment}</Text>

                    </Text>
                </View>
            ))
        }
    </>
)

const styles = StyleSheet.create({
    story: {
        width: 35,
        height: 35,
        borderRadius: 50,
        marginLeft: 10,
        borderWidth: 1.5,
        borderColor: "#FF8501"
    }
    ,
    postImage: {
        height: '100%',
        resizeMode: 'cover'
    },
    icon: {
        width: 33, height: 33
    },
    leftFooterIconsContainer: {
        width: '32%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})

export default Post