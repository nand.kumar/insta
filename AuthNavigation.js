import React, { useState, useEffect } from 'react'
import { SignedInStack, SignedOutStack } from './Navigation'
import { onAuthStateChanged } from "firebase/auth";
import { auth } from './Firebase';
const AuthNavigation = () => {
    const [currentUser, setCurrentUser] = useState(null);

    useEffect(() => {
        onAuthStateChanged(auth, (user) => {
            if (user) {
                setCurrentUser(user);
            } else {
                setCurrentUser(null);
            }
        });

    }, [])

    return (
        <>
            {currentUser ? <SignedInStack /> : <SignedOutStack />}
        </>
    )
}

export default AuthNavigation