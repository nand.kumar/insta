import { View, Text, SafeAreaView, StyleSheet, StatusBar, Platform } from 'react-native'
import React from 'react'
import AddNewPost from '../components/NewPost/AddNewPost';

const NewPostScreen = ({ navigation }) => {
    return (
        <SafeAreaView style={SafeViewAndroid.AndroidSafeArea}>
            <AddNewPost navigation={navigation} />
        </SafeAreaView>
    )
}

const SafeViewAndroid = StyleSheet.create({
    AndroidSafeArea: {
        flex: 1,
        backgroundColor: 'black',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        // marginTop: StatusBar.currentHeight,
    }
});

export default NewPostScreen