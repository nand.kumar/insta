import { View, Text, SafeAreaView, StyleSheet, Platform, StatusBar, ScrollView, Image, } from 'react-native'

import React from 'react'
import LoginForm from '../components/LoginScreen/LoginForm'

const INSTAGRAM_LOGO = "https://1000logos.net/wp-content/uploads/2017/02/Instagram-Logo.png"

const LoginScreen = ({ navigation }) => {
    return (
        <View style={SafeViewAndroid.AndroidSafeArea}>
            <View style={styles.logoContainer}>
                <Image

                    source={{
                        uri: INSTAGRAM_LOGO,
                        width: 100,
                        height: 100,
                    }}
                />

            </View>
            <LoginForm navigation={navigation} />
        </View>
    )
}

const styles = StyleSheet.create({
    logoContainer: {
        alignItems: 'center',
        marginTop: 60
    }
})

const SafeViewAndroid = StyleSheet.create({
    AndroidSafeArea: {
        flex: 1,
        backgroundColor: 'white',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        paddingHorizontal: 12
        // marginTop: StatusBar.currentHeight,
    }
});
export default LoginScreen