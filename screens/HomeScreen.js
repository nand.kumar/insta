import { SafeAreaView, StyleSheet, Platform, StatusBar, ScrollView, } from 'react-native'
import React, { useEffect, useState } from 'react'
import Header from '../components/Home/Header';
import Stories from '../components/Home/Stories';
import Post from '../components/Home/Post';
import { POSTS } from '../data/PostData';
import BottomTabs from '../components/Home/BottomTabs';
import { collection, getDocs, collectionGroup, query, orderBy } from "firebase/firestore";
import { db } from '../Firebase';

const HomeScreen = ({ navigation }) => {
    const [posts, setPosts] = useState([]);

    const getAllPosts = async () => {
        const postArray = [];
        try {
            const museums = query(collectionGroup(db, 'posts'),
                 orderBy("createdAt","desc")
            );
            const querySnapshot = await getDocs(museums);
            querySnapshot.forEach((post) => {
                postArray.push({ id: post.id, ...post.data() });
            });
            setPosts(postArray)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        console.log("hello")
        getAllPosts();
    }, [])

    return (
        <SafeAreaView style={SafeViewAndroid.AndroidSafeArea}>
            <Header navigation={navigation} getAllPosts={getAllPosts} />
            <Stories />
            <ScrollView>

                {
                    posts && posts.map((post, index) => (

                        <Post key={index} post={post} getAllPosts={getAllPosts} />
                    ))
                }
            </ScrollView>
            <BottomTabs />
        </SafeAreaView>
    )
}


const SafeViewAndroid = StyleSheet.create({
    AndroidSafeArea: {
        flex: 1,
        backgroundColor: 'black',
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
        // marginTop: StatusBar.currentHeight,
    }
});
export default HomeScreen